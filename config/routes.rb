RailsBpm::Engine.routes.draw do

  root 'dashboard#index'

  resources :process_definitions
  resources :task_definitions
  resources :roles
  resources :users, only: [ :index, :edit, :update ]

end
