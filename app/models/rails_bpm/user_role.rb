module RailsBpm
  class UserRole < ActiveRecord::Base
    belongs_to :user, class_name: RailsBpm.user_class_name, foreign_key: :user_id
    belongs_to :rails_bpm_role, class_name: RailsBpm::Role.name, foreign_key: :rails_bpm_role_id
  end
end
