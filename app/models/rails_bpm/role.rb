module RailsBpm
  class Role < ActiveRecord::Base
    validates :name, presence: true, uniqueness: true

    has_many :rails_bpm_user_roles, class_name: RailsBpm::UserRole.name, foreign_key: :rails_bpm_role_id
    has_many :users, through: :rails_bpm_user_roles, class_name: RailsBpm.user_class_name
  end
end
