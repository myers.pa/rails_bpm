module RailsBpm
  module HasBpmRoles
    extend ActiveSupport::Concern

    included do
      has_many :rails_bpm_user_roles, class_name: RailsBpm::UserRole.name
      has_many :rails_bpm_roles, through: :rails_bpm_user_roles
    end

    def bpm_roles
      self.rails_bpm_roles
    end
  end
end
