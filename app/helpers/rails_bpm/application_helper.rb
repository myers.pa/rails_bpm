module RailsBpm
  module ApplicationHelper

    def active_controller_class(controller)
      params[:controller] == ("rails_bpm/" + controller) ? 'active' : ''
    end

    def user_display(user)
      user.send(RailsBpm.user_display_method)
    end

    def user_roles(user)
      rs = user.bpm_roles.map { |r| r.name }.sort_by { |n| n }.join(", ")
      return "None" if rs.size == 0
      rs.size > 50 ? (rs[0..50] + "...") : rs
    end

    def svg_icon(icon)
      ics = "icon-#{icon.to_s}"
      content_tag :svg, class: "icon #{ics}", viewBox: "0 0 1024 1024" do
        content_tag :use, { "xlink:href" => "##{ics}" } do
        end
      end
    end

    def paginator(collection)
      render partial: "rails_bpm/shared/paginator", locals: { collection: collection }
    end

    def resource_crumbs()
      c_name = params[:controller].gsub("rails_bpm/", "").capitalize.pluralize
      crumbs = [
        [ c_name, url_for( controller: params[:controller], action: :index ).html_safe, false ],
        [ params[:action].capitalize, nil, true ]
      ]
      render partial: "rails_bpm/shared/breadcrumbs", locals: { crumbs: crumbs }
    end

    def alert(msg, priority)
      # <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      content_tag(:div, class: "alert alert-#{priority} alert-dismissible", role: "alert") do
        content_tag(:button, type: "button", class: "close", :'data-dismiss' => "alert", :'aria-label' => "Close") do
          content_tag(:span, :'aria-hidden' => true) do
            "&times;".html_safe
          end
        end +
        msg
      end
    end

    def user_select_options(users, selected = [])
      options_for_select(users.collect { |u| [ user_display(u), u.id ] }, selected.collect { |u| u.id })
    end

  end
end
