module RailsBpm
  class RolesController < RailsBpm::ApplicationController

    def index
      @roles = Role.paginate(page: params[:page], per_page: 10)
    end

    def new
      @role = Role.new
      @users = RailsBpm.user_class.all
      @selected_users = @role.users
    end

    def edit
      @role = Role.find(params[:id])
      @users = RailsBpm.user_class.all
      @selected_users = @role.users
    end

    def create
      @role = Role.new(role_params[:role])
      users = params.permit(:users).reject { |u| u.empty? }
      @role.users = users.to_a
      @selected_users = @role.users
      if(@role.save)
        flash[:success] = "Created role '#{@role.name}'!"
        redirect_to action: :index
      else
        errors = extract_errors(@role)
        flash.now[:danger] = "Couldn't save your new role...<br />#{errors.join("<br />")}".html_safe
        render :new
      end
    end

    def update
      @role = Role.find(params[:id])
      @selected_users = @role.users
      if(@role.update(role_params[:role]))
        flash[:success] = "Updated '#{@role.name}'!"
        redirect_to action: :index
      else
        errors = extract_errors(@role)
        flash.now[:danger] = "Couldn't save your role...<br />#{errors.join("<br />")}".html_safe
        render :edit
      end
    end

    def destroy
      role = Role.find(params[:id])
      if(role.destroy)
        flash[:success] = "Role '#{role.name}' deleted."
        redirect_to action: :index
      else
        flash.now[:danger] = "Sorry...couldn't delete '#{@role.name}'."
        redirect_to action: :index
      end
    end

    def role_params
      params.permit(role: [:name, :description] )
    end

  end
end
