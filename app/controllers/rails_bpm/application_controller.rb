module RailsBpm
  class ApplicationController < ActionController::Base

    def extract_errors(entity)
      errors = []
      entity.errors.each do |k,v|
        errors << "#{k.to_s.humanize} #{v}."
      end
      errors
    end

  end
end
