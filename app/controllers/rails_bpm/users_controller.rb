require_dependency "rails_bpm/application_controller"

module RailsBpm
  class UsersController < ApplicationController

    def index
      @users = RailsBpm.user_class.paginate(page: params[:page], per_page: 10)
    end

    def edit
      @user = RailsBpm.user_class.find(params[:id])
      @user_roles = @user.bpm_roles
      @roles = RailsBpm::Role.all.order(:name)
    end

    def update
      user = RailsBpm.user_class.find(params[:id])
      user.rails_bpm_roles = RailsBpm::Role.find(params[:roles])
      if(user.save)
        flash[:success] = "Successfully updated!"
      else
        flash[:error] = "Something went wrong."
      end

      redirect_to action: :edit
    end

  end
end
