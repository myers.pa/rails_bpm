
$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "rails_bpm/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "rails_bpm"
  s.version     = RailsBpm::VERSION
  s.authors     = [""]
  s.email       = [""]
  s.homepage    = "https://gitlab.com/myers.pa/rails_bpm"
  s.summary     = "A BPM tool packaged into a Rails engine for portability."
  s.description = "Rails BPM provides an easy to use BPM tool for managing workflows and system processes.
  Packaged in a Rails engine, Rails BPM integrates easily into existing applications and with existing user models."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib,vendor}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.4"
  s.add_dependency "jquery-rails"
  s.add_dependency "sass-rails", ">= 3.2"
  s.add_dependency "turbolinks"
  s.add_dependency "audited-activerecord", "~> 4.0"
  s.add_dependency "bootstrap-sass", '~> 3.3.1'
  s.add_dependency 'autoprefixer-rails'
  s.add_dependency 'will_paginate', '~> 3.0.6'
  s.add_dependency 'haml'
  s.add_dependency 'select2-rails'

  s.add_development_dependency "sqlite3"
end
