require "rails_bpm/engine"

module RailsBpm
  mattr_accessor :title

  mattr_accessor :user_class_name
  mattr_accessor :user_display_method
  mattr_accessor :current_user_method

  def self.title
    @@title || 'Rails BPM'
  end

  def self.user_class
    @@user_class_name.constantize
  end

  def self.user_display_method
    @@user_display_method || :email
  end

  def self.current_user_method
    @@current_user_method || :current_user
  end
end
