# This should be auto-included with the
# Bundler.require :default, Rails.env, but it's not happening
# Require them here so we can get them stuck in the asset pipeline
# from the engine
require "jquery-rails"
require "turbolinks"
require "bootstrap-sass"
require "select2-rails"

require 'audited-activerecord'
require 'will_paginate'

require 'haml'

module RailsBpm
  class Engine < ::Rails::Engine
    isolate_namespace RailsBpm

    # Add the engine migrations to the rails migration path
    # so we don't have to copy them over to the host app
    initializer :append_migrations do |app|
      unless app.root.to_s == root.to_s
        config.paths["db/migrate"].expanded.each do |expanded_path|
          app.config.paths["db/migrate"] << expanded_path
        end
      end
    end

  end
end
