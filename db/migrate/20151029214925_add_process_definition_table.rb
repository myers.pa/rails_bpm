class AddProcessDefinitionTable < ActiveRecord::Migration
  def up
    create_table :rails_bpm_process_definitions do |t|
      t.integer :author
      t.string :name
      t.text :description

      t.timestamps
    end
  end

  def down
    drop_table :rails_bpm_process_definitions
  end
end
