class RoleTables < ActiveRecord::Migration
  def up
    create_table :rails_bpm_roles do |t|
      t.string :name
      t.text :description

      t.timestamps
    end

    create_table :rails_bpm_user_roles do |t|
      t.belongs_to :user
      t.belongs_to :rails_bpm_role

      t.timestamps
    end
  end

  def down
    drop_table :rails_bpm_roles
    drop_table :rails_bpm_user_roles
  end
end
