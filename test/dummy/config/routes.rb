Rails.application.routes.draw do
  get 'welcome/index'
  get 'welcome/protected'

  devise_for :users
  
  root to: 'welcome#index'

  mount RailsBpm::Engine => "/rails_bpm" 
end
