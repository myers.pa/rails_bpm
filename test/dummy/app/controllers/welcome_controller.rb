class WelcomeController < ApplicationController
  before_action :authenticate_user!, only: [:protected]

  def index
    @user = current_user
  end

  def protected
    @user = current_user
  end
end
